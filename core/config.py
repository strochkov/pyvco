import argparse
import lib
import os
import copy
import logging

_logger = logging.getLogger(__name__)

class Config:
    _cmdline_args = None
    _override_filename = None
    _current_dir = None

    def get(self):
        if self._cmdline_args is None:
            self._cmdline_args = self.get_cmdline_args()
        args = copy.copy(self._cmdline_args)
        if self._override_filename is not None:
            config = lib.ConfigObj(self._override_filename)
            args.__dict__.update(config)
        _logger.debug('Config.get: _current_dir: %s, _override_filename: %s\n'+'Returned arguments: %s',
                      self._current_dir, self._override_filename, str(args))
        return args

    @staticmethod
    def get_cmdline_args():
        parser = argparse.ArgumentParser(description='Batch video converter.')
        parser.add_argument('target_dir', help='target directory')
        parser.add_argument('-o',
                            dest='output_dir',
                            help='output directory (default: ./output)',
                            default='./output')
        parser.add_argument('-p',
                            dest='should_preserve_dir_structure',
                            action='store_true',
                            help='flag: preserve directory structure (default: false)')
        parser.add_argument('-al',
                            dest='audio_lang',
                            help='audio language (default: rus)',
                            default='rus')
        parser.add_argument('-sl',
                            dest='subtitles_lang',
                            help='subtitles language (default: none)',
                            default='none')
        parser.add_argument('-t',
                            dest='test_run',
                            action='store_true',
                            help='test run, 5 min limit to each file (default: false)')
        parser.add_argument('-ffmpeg',
                            dest='ffmpeg',
                            help='path to ffmpeg (default: ./ffmpeg/ffmpeg.exe)',
                            default='./ffmpeg/ffmpeg.exe')
        parser.add_argument('-ffprobe',
                            dest='ffprobe',
                            help='path to ffprobe (default: ./ffmpeg/ffprobe.exe)',
                            default='./ffmpeg/ffprobe.exe')
        parser.add_argument('-d',
                            dest='debug',
                            action='store_true',
                            help='Debug mode - more verbosity for logger (default: false)')
        return parser.parse_args()

    def set_current_dir(self, current_dir):
        current_dir_config_filename = os.path.join(current_dir, '.vc')
        need_to_reload_config = False
        if (self._current_dir is not None) and (not current_dir.startswith(self._current_dir)):
            self._current_dir = None
            self._override_filename = None
            need_to_reload_config = True
        if os.path.isfile(current_dir_config_filename):
            self._current_dir = current_dir
            self._override_filename = current_dir_config_filename
            need_to_reload_config = True
        return need_to_reload_config
