import smtplib
from email.mime.text import MIMEText


class Mailer:
    _server = None
    _port = 465
    _login = None
    _password = None

    def __init__(self, server, port, login, password):
        self._server = server
        self._port = port
        self._login = login
        self._password = password

    def send(self, to, subject, message):
        msg = MIMEText(message)
        msg['Subject'] = subject
        msg['From'] = self._login
        msg['To'] = to

        s = smtplib.SMTP_SSL(self._server, self._port)
        s.login(self._login, self._password)
        s.sendmail(self._login, [to], msg.as_string())
        s.quit()
