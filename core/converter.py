import lib
import os
import logging

_logger = logging.getLogger(__name__)

class ConvertError(Exception):
    def __init__(self, message='?', cmd='?', output='?', details=None, pid=0):
        """
        @param    message: Error message.
        @type     message: C{str}

        @param    cmd: Full command string used to spawn ffmpeg.
        @type     cmd: C{str}

        @param    output: Full stdout output from the ffmpeg command.
        @type     output: C{str}

        @param    details: Optional error details.
        @type     details: C{str}
        """
        super(ConvertError, self).__init__(message)

        self.cmd = cmd
        self.output = output
        self.details = details
        self.pid = pid

    def __repr__(self):
        error = self.details if self.details else self.message
        return ('<ConvertError error="%s", pid=%s, cmd="%s">' %
                (error, self.pid, self.cmd))

    def __str__(self):
        return self.__repr__()


class Converter:
    def convert(self, filename, options):
        ffmpeg = lib.FFMpeg(options.ffmpeg, options.ffprobe)
        info = ffmpeg.probe(filename)
        if not isinstance(info, lib.MediaInfo):
            return
        _logger.debug('FFMpeg streams: %s', str(info.streams))
        video_stream = self.select_video_stream(info)
        audio_stream = self.select_audio_stream(info, options.audio_lang)
        subtitles_stream = self.select_subtitles_stream(info, options.subtitles_lang)
        if subtitles_stream is None:
            subtitles_file = self.select_subtitles_file(os.path.dirname(filename), options.subtitles_lang)
        else:
            subtitles_file = filename

        ff_options = self._get_ff_video_options(video_stream) \
                     + self._get_ff_audio_options(audio_stream) \
                     + self._get_ff_subtitles_options(subtitles_stream, subtitles_file, info)

        if options.test_run:
            ff_options.extend(['-t', '300'])

        output_filename = self._get_output_filename(filename, options)
        if not os.path.exists(os.path.dirname(output_filename)):
            os.makedirs(os.path.dirname(output_filename))

        progress = ffmpeg.convert(filename, output_filename, ff_options)
        print(output_filename)
        try:
            for time in progress:
                print(time)
        except lib.FFMpegConvertError as e:
            raise ConvertError(e.message, e.cmd, e.output, e.details, e.pid)
        except lib.FFMpegError as e:
            raise ConvertError(e.message)

        return

    @staticmethod
    def _get_ff_video_options(video_stream):
        options = []
        if video_stream is not None:
            options.extend([
                '-map', '0:' + str(video_stream.index),
                '-c:v', 'libx264',
                '-preset', 'veryfast',
                '-crf', '27',
                '-f', 'mp4'
            ])
        return options

    @staticmethod
    def _get_ff_audio_options(audio_stream):
        options = []
        if audio_stream is not None:
            options.extend([
                '-map', '0:' + str(audio_stream.index)
            ])
        return options

    @staticmethod
    def _get_ff_subtitles_options(subtitles_stream, subtitles_filename, info):
        options = []
        subtitles_index_option = ''
        if subtitles_stream is not None:
            subtitles_index = 0
            for stream in info.streams:
                if stream.type == 'subtitle':
                    if stream.index == subtitles_stream.index:
                        break
                    subtitles_index += 1
            subtitles_index_option = ':si=' + str(subtitles_index)

        if subtitles_filename is not None:
            options.extend([
                '-vf',
                'subtitles=\'' + subtitles_filename.replace('\\', '\\\\').replace(':',
                                                                                  '\:') + '\'' + subtitles_index_option
            ])

        return options

    @staticmethod
    def _get_output_filename(filename, options):
        if options.should_preserve_dir_structure:
            out_filename = filename.replace(options.target_dir, '')
            if out_filename.startswith(('\\', '/')):
                out_filename = out_filename[1:]
        else:
            out_filename = os.path.basename(filename)
        out_filename = os.path.splitext(out_filename)[0] + '.mp4'
        return os.path.join(options.output_dir, out_filename)

    @staticmethod
    def select_video_stream(info):
        return info.video

    @staticmethod
    def select_audio_stream(info, lang):
        selected_stream = Converter._select_stream_by_lang_and_title('audio', lang, info)
        return selected_stream if selected_stream is not None else info.audio

    @staticmethod
    def select_subtitles_stream(info, lang):
        return Converter._select_stream_by_lang_and_title('subtitle', lang, info)

    @staticmethod
    def _select_stream_by_lang_and_title(type, lang, info):
        selected_stream = None
        for stream in info.streams:
            if (hasattr(stream, 'type')) and (hasattr(stream, 'metadata')) and ('language' in stream.metadata):
                if (stream.type == type) and ((stream.metadata['language'].lower().find(lang.lower())) != -1):
                    has_title_shorter = False
                    if (selected_stream is not None) and ('title' in stream.metadata) and (
                                'title' in selected_stream.metadata):
                        if len(stream.metadata['title']) < len(selected_stream.metadata['title']):
                            has_title_shorter = True
                    if selected_stream is None or has_title_shorter:
                        selected_stream = stream

        return selected_stream

    @staticmethod
    def select_subtitles_file(directory, lang):
        selected_file = None
        for subdir, dirs, files in os.walk(directory):
            for filename in files:
                if filename.endswith('.srt') and filename.lower().find(lang.lower()) != -1:
                    selected_file = os.path.join(subdir, filename)
            break
        return selected_file
