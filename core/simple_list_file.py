class SimpleListFile:
    _filename = None
    _converted_files = None

    def __init__(self, filename):
        self._filename = filename
        f = open(filename, 'a+')
        self._converted_files = f.readlines()
        f.close()
        return

    def is_in_list(self, filename):
        return filename+'\n' in self._converted_files

    def add(self, filename):
        self._converted_files.append(filename)
        f = open(self._filename, 'a+')
        f.write(filename+'\n')
        f.close()
        return
