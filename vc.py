from __future__ import print_function
import core
import os
import logging

config = core.Config()
args = config.get()
logging.basicConfig(level=logging.DEBUG if args.debug else logging.ERROR, format='#### %(asctime)s %(message)s')
logger = logging.getLogger(__name__)
logger.debug('Os is POSIX: %s', os.name is 'posix')
config_params = config.get()
converted_files = core.SimpleListFile('converted_files')

for subdir, dirs, files in os.walk(args.target_dir):
    need_to_reload_config = config.set_current_dir(subdir)
    if need_to_reload_config:
        config_params = config.get()
    for filename in files:
        if filename.endswith(('.avi', '.flv', '.m4v', '.mkv', '.mov', '.mp4', '.mpeg', '.mpg', '.wmv')):
            full_filename = os.path.join(subdir, filename)
            if not converted_files.is_in_list(full_filename):
                try:
                    core.Converter().convert(full_filename, config_params)
                except core.ConvertError as e:
                    logger.error('Convert error: %s\n'
                                 '## Cmd: %s\n'
                                 '## Details: %s\n'
                                 '## Output: %s', e.message, e.cmd, e.details.encode('utf-8'), e.output.encode('utf-8'))
                else:
                    logger.debug('Success: %s', full_filename)
                    converted_files.add(full_filename)